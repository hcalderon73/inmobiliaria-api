'use strict'

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const http = require('http');
const socketIo = require('socket.io');
const server = http.Server(app);
const io = socketIo(server);
const _ = require('lodash');

// cargar rutas.
const user_routes = require('./routes/user');
const client_routes = require('./routes/client');

// middleware de body-parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Configurar cabecera y cors
app.use(cors());

// rutas body-parser
app.use('/api', [user_routes, client_routes]);

// server io.
server.listen(3000);

// Chat.    
let usersList = [];
let usersSelect = [];
let userConnect = {};
let chat = '';

io.on("connection", (socket) => {
    // Obtenemos la conexion al socket;

    // IP
    const ipAddress = socket.handshake.address;
    // Browser
    const browser = socket.request.headers;
    // ID SOCKET
    const socketId = socket.id;

    socket.on('new chat' , (data) => {
        const chatId = data.userSelect.id;
        const chatName = chatId + ' room';
        socket.join(chatName);
        console.log(socket.rooms);
       // io.to(chatName).emit('some event', { message: 'chat ' + chatName });
        socket.broadcast.to(data.userSelect.socketId).emit('chatRoom', { message: 'hey' });
       /* const createBy = data.userName;
        const date = new Date();
        const usersChat = data.users;*/
        socket.emit('new chat', {
            charRoom: chatName,
        });

    });

    socket.on('message room', (data) => {
        io.to(data.userSelect.socketId).emit('chatRoom', { message:data.message });
    });

    socket.on('users select' , (data) => {
        const chatId = data.chatId;
        const createBy = data.userName;
        const usersChat = data.users;
        const date = new Date();
    });


    // Cuando se conecta un nuevo usuario.
    socket.on("new user", (user) => {
        userConnect = user;

        updateUsers(userConnect);
        
        usersList.push({
            userName: user.userName,
            id: user.userId,
            avatar: user.userAvatar,
            browser: browser,
            ip: ipAddress,
            socketId: socketId,
            connectDate: new Date()
        })

        io.emit("users connect", {
            usersList
        });

        socket.emit('chatRoom', {
            message: 'Bienvenido al chat ' + user.userName
        });

        socket.broadcast.emit('user connect', {
            message: user.userName + ' se ha conectado'
        });
    });

    function updateUsers(data) {
        _.remove(usersList, function (n) {
            return n.id == data.userId;
        });
    }

    socket.on("disconnect user", (user) => {
        updateUsers(user)

        socket.emit('chatRoom', {
            message: 'Sale del  chat ' + user.userName
        })

        socket.broadcast.emit('user connect', {
            message: user.userName + ' se ha desconectado'
        })

        io.emit("users connect", {
            usersList
        }) 
    })



    socket.on("disconnect", () => {

    })
})

module.exports = app;
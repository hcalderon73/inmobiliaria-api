'use strict'

var Client = require('../models/client');

function createClient(req, res) {
    var client = new Client();
    var params = req.body;

    client.firstname = params.firstname;
    client.lastname = params.lastname;
    client.email = params.email;
    client.document_id = params.document_id;
    client.phone = params.phone;
    client.mobile = params.mobile;
    client.whatsap = params.whatsap;
    client.address_id = params.address_id;
    client.owner = params.owner;
    client.contracts = params.contracts;
    client.assigned_to = params.assigned_to;
    client.created_by = params.created_by;

    client.save((err, clientStored) => {
        if (err) {
            res.status(500).send({ message: 'Error' });
        } else {
            if (!clientStored) {
                res.status(404).send({
                    message: 'No se ha podido crear el Cliente'
                })
            } else {
                res.status(200).send({
                    client: clientStored
                })
            }
        }
    });
}

function getClients(req, res) {
    var data;
    Client.find((err, clientStored) => {
        res.status(200).send({
            clients: clientStored
        })
    })

    
}

module.exports = {
    createClient,
    getClients
}
'use strict'

const bcrypt = require('bcrypt-nodejs');
const User = require('../models/user');
const Role = require('../models/role');
const Image = require('../models/Image');
const token = require('../services/jwt');
const moment = require('moment');
const express = require('express');
const app = express();
const http = require('http');
const socketIo = require('socket.io');
const server = http.Server(app);
const fs = require('fs');
const path = require('path');
server.listen(3001);
const io = socketIo(server);




function tokenCheck(req, res) {
    res.status(200).send({
        user: req.user
    });
}


function register(req, res) {
    const user = new User();
    const params = req.body;

    user.firstname = params.firstname;
    user.lastname = params.lastname;
    user.email = params.email;
    user.password = params.password;
    user.avatar = null;
    user.role = 'user';

    User.find({ email: user.email.toLowerCase() }, (err, userCheck) => {
        if (err) {
            res.status(500).send({ message: 'Error' });
        } else {
            if (userCheck.length == 0) {
                bcrypt.hash(params.password, null, null, function (err, hash) {
                    user.password = hash;
                    user.save((err, userStored) => {
                        if (err) {
                            res.status(500).send({ message: 'Error' });
                        } else {
                            if (!userStored) {
                                res.status(404).send({
                                    message: 'No se ha podido crear el usuario'
                                })
                            } else {
                                res.status(200).send({
                                    user: userStored
                                })
                                io.on("connection", (socket) => {
                                    const agent = socket.request.headers;
                                    socket.emit('user', {
                                        user: userStored
                                    })
                                })
                            }
                        }
                    })
                })
            } else {
                res.status(200).send({
                    message: 'el usuario ya esta registrado'
                })
            }
        }
    });
}

function login(req, res) {
    var params = req.body;
    var email = params.email;
    var password = params.password;

    User.findOne({ email: email.toLowerCase() }, (err, user) => {
        if (err) {
            res.status(500).send({ message: 'Error al comprobar el usuario' });
        } else {
            if (user) {
                bcrypt.compare(password, user.password, (err, check) => {
                    if (check) {
                        // comporbar y generar token.
                        // if (params.gettoken) {
                        res.status(200).send({
                            token: token.createToken(user),
                            user: user
                        });
                        /*} else {
                            res.status(200).send({ user });
          0              }*/
                    } else {
                        res.status(404).send({
                            message: 'La contraseña es incorrecta'
                        });
                    }
                })
            } else {
                res.status(404).send({
                    message: 'Datos desconocidos'
                });
            }
        }
    })
}

function getUsers(req, res) {
    User.find({}, (err, users) => {
        if (users) {
            res.status(200).send({
                users
            })
        } else {
            res.status(404).send({
                message: 'No hay Usuarios'
            });
        }
    })
}

function getUser(req, res) {
    var params = req.body;
    var id = params.id;

    User.findOne({ _id: id }, (err, user) => {
        if (user) {
            res.status(200).send({
                user
            })
        } else {
            res.status(404).send({
                message: 'No hay Usuarios'
            });
        }
    });
}

function getRoles(req, res) {
    Role.find({}, (err, roles) => {
        if (roles) {
            res.status(200).send({
                roles
            })
        } else {
            res.status(404).send({
                message: 'No hay Roles'
            });
        }
    })
}

function update(req, res) {
    const params = req.body;
    const user_id = params.user_id;
    const userSet = params.user;

    User.findById(user_id, (err, user) => {
        if (user) {
            delete user.password;
            delete user.created_at;
            user.firstname = userSet.firstname;
            user.lastname = userSet.lastname;
            user.email = userSet.email;
            user.role = userSet.role;
            user.update_at = new Date();
            user.updated_by = userSet.updated_by;
            user.save((err, userStored) => {
                res.status(200).send({
                    user: userStored,
                    status: 'ok'
                })
            });
        }
    });
}

function uploadAvatar(req, res) {
    const user_id = req.params.id;
    const files = req.files;

    if (files) {
        const file_path = files.image.path;
        const file_split = file_path.split('\\');
        const file_name = file_split[2];
        const ext_split = file_name.split('\.');
        const file_ext = ext_split[1];
        // valida extensiones
        if (file_ext == 'png' || file_ext == 'jpg' || file_ext == 'jpeg' || file_ext == 'gif') {
            // validamos que sea el mismo usuario
            if (user_id != req.user.sub) {
                return res.status(500).send({
                    message: 'No tienes permisos para actualizar el avatar'
                })
            } else {
                // Ubicamos usuario.
                User.findById(user_id, (err, user) => {
                    if (user) {
                        // creamos avatar para el usuario
                        const avatar = new Image();
                        avatar.filename = file_name,
                            avatar.url = './uploads/users',
                            avatar.ext = file_ext,
                            avatar.description = 'description',
                            avatar.property_id = user_id

                        // El usuario Tiene avatar ?
                        Image.findOneAndRemove({ _id: user.avatar }, (err, imagen) => {
                            if (imagen) {
                                console.log(imagen);
                                const filename = imagen.filename;
                                const path_file = imagen.url + '/' + filename;
                                fs.exists(path_file, (exists) => {
                                    if (exists) {
                                        console.log('existe la ruta', path_file);
                                        fs.unlink(path_file, function () {
                                            console.log('imagen borrada');
                                        });
                                    }
                                })
                            }
                        })

                        // guardamos avatar
                        avatar.save((err, imageStored) => {
                            if (imageStored) {
                                // obtenemos id de la imagen.
                                const image_id = imageStored._id;
                                if (image_id) {
                                    // buscamos el usario y lo actualizamos con el id del avatar,
                                    User.findByIdAndUpdate(user_id, { avatar: image_id }, { new: true }, (err, userUpdated) => {
                                        if (err) {
                                            res.status(500).send({
                                                message: 'Error al Actualizar el Usuario'
                                            })
                                        }
                                        if (!userUpdated) {
                                            res.status(404).send({
                                                message: 'No se pudo actualizar el Usuario'
                                            })
                                        } else {
                                            // Todo correcto.<
                                            res.status(200).send({
                                                user: userUpdated, image: imageStored
                                            })
                                        }
                                    })
                                } else {
                                    res.status(500).send({
                                        message: 'Error al registrar Imagen'
                                    })
                                }
                            }
                        })
                    }
                });
            }
        } else {
            res.status(200).send({
                message: 'Formato no valido'
            })
        }
    } else {
        res.status(200).send({
            message: 'No hemos recibido ningun archivo'
        })
    }
}

function getImage(req, res) {
    var params = req.body;
    var id = params.id;
    Image.findOne({ _id: id }, (err, imagen) => {
        if (imagen) {
            res.status(200).send({
                imagen: imagen
            })
        } else {
            res.status(404).send({
                message: 'No hay Imagen'
            });
        }
    });
}

function getRol(req, res) {
    var params = req.params;
    var id = params.id;
    Role.findOne({ _id: id }, (err, rol) => {
        if (rol) {
            res.status(200).send({
                rol
            })
        } else {
            res.status(404).send({
                message: 'No hay Rol'
            })
        }
    })


}


function getAvatar(req, res) {
    //var params = req.body;
    var id = req.params.id;

    Image.findOne({ _id: id }, (err, imagen) => {
        if (imagen) {
            const filename = imagen.filename;
            const path_file = imagen.url + '/' + filename;
            fs.exists(path_file, (exists) => {
                if (exists) {
                    res.sendFile(path.resolve(path_file));
                } else {
                    res.status(404).send({
                        message: 'No hay imagen asociada'
                    })
                }
            })
        } else {
            res.status(404).send({
                message: 'No hay Imagen'
            });
        }
    });

}


module.exports = {
    tokenCheck,
    login,
    register,
    getUsers,
    getUser,
    getRoles,
    update,
    uploadAvatar,
    getImage,
    getAvatar,
    getRol
}
'use strict'

var mongoose = require('mongoose');
var app = require('./app');
var port = process.env.PORT || 3789;

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/real_state', { useMongoClient: true })
    .then(() => {
        app.listen(port, () => {
            console.log('el servidor local esta corriendo.');
        })
        console.log('la conexion se ha realizado correctamente... =)')
    })
    .catch(
    err => console.log(err)
    );

'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secrect = 'clave_secreta';

exports.ensureAuth = function (req, res, next) {

    // Comprobamos que existe la cabecera Authorization.
    if (!req.headers.authorization) {
        return res.status(403).send({
            message: 'La peticion no tiene la cabecera'
        })
    }

    // limpiamos token.
    var token = req.headers.authorization.replace(/['"]+/g, '');
    var parts = token.split(' ');
    if (parts.length === 2) {
        var credentials = parts[1];
    }

    try {
        // obtenemos datos del token.
        var payload = jwt.decode(credentials, secrect);
        // comprobamos valides del token.
        if (payload.exp <= moment().unix()) {
            return res.status(401).send({
                message: 'El token ha expirado'
            })
        }

    } catch (error) {
        return res.status(404).send({
            message: 'El token no es valido'
        })
    }

    // datos del usuario.
    req.user = payload;

    // continuamos.
    next();

}

exports.isAdmin = function (req, res, next) {
    if(req.user.role != 'ROLE_ADMIN') {
        return res.status(200).send({message: 'No tienes permisos para acceder a esta zona.'});
    }

    next();
}
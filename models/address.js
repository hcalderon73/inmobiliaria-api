'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AddressSchema = Schema({
    street: String,
    postalcode: String,
    city:String,
    state:String,
    country_id: { type: Schema.ObjectId, ref: 'Country' },
    latitud: Number,
    longitud: Number,
    created_at:  { type: Date, default: Date.now },
    update_at:  { type: Date, default: Date.now },
    created_by: Number
});

module.exports = mongoose.model('Address', AddressSchema);
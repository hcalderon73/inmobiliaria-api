'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ClientSchema = Schema({
    firstname: String,
    lastname: String,
    email:String,
    document_id:String,
    phone: Number,
    mobile: Number,
    whatsap: Boolean,
    address_id: Number,
    owner: Boolean,
    contracts: JSON,
    created_at:  { type: Date, default: Date.now },
    update_at:  { type: Date, default: Date.now },
    assigned_to: { type: Schema.ObjectId, ref: 'User' },
    created_by: { type: Schema.ObjectId, ref: 'User' }
});

module.exports = mongoose.model('Client', ClientSchema);
'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ContractSchema = Schema({
    type_contract: { type: Schema.ObjectId, ref: 'TypeContract' },
    type_property: { type: Schema.ObjectId, ref: 'TypeProperty' },
    created_at:  { type: Date, default: Date.now },
    update_at:  { type: Date, default: Date.now },
    created_by: { type: Schema.ObjectId, ref: 'User' },
});

module.exports = mongoose.model('Contract', ContractSchema);
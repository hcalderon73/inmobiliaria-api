'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ImageSchema = Schema({
    filename: String,
    url:String,
    ext: String,
    description: String,
    property_id: { type: Schema.ObjectId, ref: 'User' },
    created_at:  { type: Date, default: Date.now },
    update_at:  { type: Date, default: Date.now },
});

module.exports = mongoose.model('Image', ImageSchema);
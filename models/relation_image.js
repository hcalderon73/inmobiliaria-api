'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RelationImageSchema = Schema({
    image_id: { type: Schema.ObjectId, ref: 'Image' },
    property_id: { type: Schema.ObjectId, ref: 'Property' },
});

module.exports = mongoose.model('RelationImage', RelationImageSchema);
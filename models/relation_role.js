'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var RelationRoleSchema = Schema({
    user_id: { type: Schema.ObjectId, ref: 'User' },
    role_id: { type: Schema.ObjectId, ref: 'Role' },
});

module.exports = mongoose.model('RelationRole', RelationRoleSchema);
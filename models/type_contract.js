'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TypeContractSchema = Schema({
    name: String,
    description: String,
});

module.exports = mongoose.model('TypeContract', TypeContractSchema);
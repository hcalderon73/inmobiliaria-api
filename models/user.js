'use strict'

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = Schema({
    firstname: String,
    lastname: String,
    email:String,
    password:String,
    avatar: String,
    created_at: { type: Date, default: Date.now },
    update_at: { type: Date, default: Date.now },
    role: { type: Schema.ObjectId, ref: 'Role' },
    created_by: { type: Schema.ObjectId, ref: 'User' },
    updated_by: { type: Schema.ObjectId, ref: 'User' }        

});

module.exports = mongoose.model('User', UserSchema);
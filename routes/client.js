'use strict'

var express = require('express');
var ClientController = require('../controllers/client');
var api = express.Router();
var m_auth = require('../middlewares/authenticated');

api.post('/client_create', [m_auth.ensureAuth, m_auth.isAdmin], ClientController.createClient);
api.get('/clients', m_auth.ensureAuth, ClientController.getClients);

module.exports = api;
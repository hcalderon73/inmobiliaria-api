'use strict'

const express = require('express');
const UserController = require('../controllers/user');
const api = express.Router();
const m_auth = require('../middlewares/authenticated');
const multiparty = require('connect-multiparty');
const m_upload = multiparty({uploadDir: './uploads/users'});


api.get('/token', m_auth.ensureAuth, UserController.tokenCheck);
api.post('/login', UserController.login);
api.post('/register', UserController.register);
api.post('/user', m_auth.ensureAuth, UserController.getUser);
api.post('/user_set', m_auth.ensureAuth, UserController.update);
api.get('/users', m_auth.ensureAuth, UserController.getUsers);
api.get('/roles', m_auth.ensureAuth, UserController.getRoles);
api.post('/avatar', m_auth.ensureAuth, UserController.getImage);
api.post('/update_avatar/:id', [m_auth.ensureAuth, m_upload], UserController.uploadAvatar);
api.get('/get_avatar/:id', UserController.getAvatar);
api.get('/get_rol/:id', UserController.getRol);


module.exports = api;
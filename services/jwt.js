'use strict'

var jwt = require('jwt-simple');
var moment = require('moment');
var secrect = 'clave_secreta';

exports.createToken = function(user) {
    var payload = {
        sub: user._id,
        nombre: user.firstname,
        surname: user.lastname,
        email: user.email,
        role: user.role,
        avatar: user.avatar,
        iat: moment().unix(),
        exp: moment().add(30, 'days').unix()
    };

    return jwt.encode(payload, secrect);
};